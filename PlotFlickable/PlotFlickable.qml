import QtQuick 2.9
import Ubuntu.Components 1.3

// Original code from https://github.com/oosavu/plotter

//элемент позволяет интерактивно работать с графиком (изменять размер, проматывать)
// the element allows you to interactively work with the chart (resize, scroll)
Flickable {
    id: flick
    contentWidth: width
    contentHeight: height
    boundsBehavior: Flickable.StopAtBounds
    property alias canvas: canvas
    clip: true

    onWidthChanged: {
        if(contentWidth < width) contentWidth = width
        canvas.requestPaint()
    }
    onHeightChanged: canvas.requestPaint()

    Rectangle{
        color: "white"
        width: flick.contentWidth
        height: flick.contentHeight
        PlotCanvas{
            anchors.fill: parent
            id: canvas
        }
    }

    MouseArea{
        anchors.fill: parent
        // изменение размера графика
        // resize the chart
        onWheel:{
            var prevPersent = ( wheel.x)/(flick.contentWidth)
            var coef = wheel.angleDelta.y < 0? 0.98 : 1.02
            if(flick.contentWidth * coef < flick.width) flick.contentWidth = flick.width
            else flick.contentWidth = flick.contentWidth * coef
            var newContentX = prevPersent*flick.contentWidth - wheel.x + flick.contentX
            if(newContentX < 0) newContentX = 0
            if(newContentX > flick.contentWidth - flick.width) newContentX =flick.contentWidth - flick.width
            flick.contentX = newContentX
            flick.returnToBounds()
            canvas.requestPaint()
        }
        // клик по графику
        // click on the chart
        onClicked: {
            console.log(flick.contentX, flick.contentWidth, flick.width)
            canvas.click(mouse.x , mouse.y)
            canvas.requestPaint()
        }
    }
}
