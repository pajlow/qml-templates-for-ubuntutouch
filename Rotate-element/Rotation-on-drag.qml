/*
 * Copyright (C) 2020  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    id: root
    width: units.gu(40)
    height: width

    Item {
        id: thePiece
        anchors.fill: parent

        property real angle: 0

        Rectangle {
            anchors.centerIn: parent
            width: units.gu(30)
            height:width
            color: "#110011"
        }

        //Other elements to rotate, here

        rotation: drag.distance
    }

    //MouseArea needs to be outside of the element that rotates
    MouseArea {
        id: drag
        anchors.fill: parent

        property real distance: 0
        property int pressedX: 0
        property int pressedY: 0
        property bool dragging: false

        onPressed: {
            dragging = true
            pressedX = mouseX
            pressedY = mouseY
        }

        onReleased: {
            //Keep track of the current angle for the next interaction
            thePiece.angle = distance
            dragging = false
            pressedX = 0
            pressedY = 0
        }

        onPositionChanged: {
            //Previous angle + atan2 from the center of the root to the updated mouse position - atan2 from the center of the piece to the first tap
            distance = (thePiece.angle + (Math.atan2(mouseY - root.height / 2, mouseX - root.height / 2) - Math.atan2(drag.pressedY - root.width / 2, drag.pressedX - root.width / 2)) * 180 / Math.PI) % 360;
        }
    }
}
