/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'importexample.cibersheep'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    PageStack {
        id: mainStack
        anchors.fill: parent
    }

    Page {
        id: mainPage
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Import Example')
        }

        Button {
            anchors.centerIn: parent
            text: i18n.tr('Import a file')

            onClicked: {
                var importPage = mainStack.push(Qt.resolvedUrl("PageImport.qml"), {"allowMultipleFiles":false});

                importPage.close.connect(function() {
                    mainStack.pop();
                });

                importPage.accept.connect(function(urlOfItems) {
                    for (var i = 0; i < urlOfItems.length; i++) {
                        console.log("Imported File Url: ", urlOfItems[i]);
                    }
                });
            }
        }
    }

    Component.onCompleted: mainStack.push(mainPage);
}
